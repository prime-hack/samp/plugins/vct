#include "main.h"
#include <ImWrapper/ImWrapper.hpp>

#define CONFIG _PRODJECT_NAME ".ini"

AsiPlugin::AsiPlugin() : SRDescent(), CIniSerialization( CONFIG ) {
	// Constructor
	loaderSlots =
		ImGui::ImLoader::CreateInstance( g_class.DirectX->d3d9_device(), g_vars.hwnd )
			->connectToCallbacks( g_class.DirectX->onPreReset, g_class.DirectX->onPostReset, g_class.DirectX->onDraw );

	// Загрузка данных из файла настроек
	memsafe::write( 0x524BB4, &max_fov() );
	memsafe::write( 0x524BC5, max_fov() );
	*(float *)0x8CC604 = distance();
	*(float *)0x8CC634 = Angle__min();
	*(float *)0x8CC638 = Angle__max();
	*(float *)0x8CC600 = Height__mul();
	*(float *)0x8CC608 = Height__sub();

	// Создание меню
	menu = new ImMenu( PROJECT_NAME );
	menu->onHide += [this] {
		g_class.cursor->hideCursor();
		// Синхронизация данных из игровой структуры с плагином
		distance()	  = *(float *)0x8CC604;
		Angle__min()  = *(float *)0x8CC634;
		Angle__max()  = *(float *)0x8CC638;
		Height__mul() = *(float *)0x8CC600;
		Height__sub() = *(float *)0x8CC608;
		// Синхронизация данных с файлом настроек
		sync();
	};

	// Автоматическое возвращение камеры ВЗАД автомобиля
	auto rest = new ImCheckBox( "Restore camera on driving", &restore() );
	mid		  = g_class.events->onMainLoop += [this] {
		  if ( !restore() ) memsafe::write( 0xB70118, 50.0f );
	};

	// Максимальный угол обзора при разгоне
	auto fov = new ImDragFloat( "Max FOV", &max_fov() );
	fov->onFloatChanged += []( float val ) { memsafe::write( 0x524BC5, val ); };
	fov->min   = 70;
	fov->max   = 150;
	fov->speed = 1;

	// Дистанция от камеры до авто
	auto dist	= new ImDragFloat( "Distance", (float *)0x8CC604 );
	dist->min	= -3;
	dist->max	= 30;
	dist->speed = 0.1f;
	dist->power = 0.001f;

	// Угол на который можно опустить камеру
	auto minAngle	= new ImDragFloat( "Min angle", (float *)0x8CC634 );
	minAngle->min	= -1.553343058;
	minAngle->max	= 1.553343058;
	minAngle->speed = 0.01f;
	minAngle->power = 0.001f;

	// Угол на который можно поднять камеру
	auto maxAngle	= new ImDragFloat( "Max angle", (float *)0x8CC638 );
	maxAngle->min	= -1.553343058;
	maxAngle->max	= 1.553343058;
	maxAngle->speed = 0.01f;
	maxAngle->power = 0.001f;

	// Множитель высоты камеры
	auto hiMul	 = new ImDragFloat( "Height mul", (float *)0x8CC600 );
	hiMul->min	 = 0;
	hiMul->max	 = 30;
	hiMul->speed = 0.1f;
	hiMul->power = 0.001f;

	// Вычитатель высоты камеры
	auto hiSub	 = new ImDragFloat( "Height sub", (float *)0x8CC608 );
	hiSub->min	 = -30;
	hiSub->max	 = 3;
	hiSub->speed = 0.1f;
	hiSub->power = 0.001f;

	// Запихиваем созданные дергалки и переключалки в меню
	menu->chields.push_back( rest );
	menu->chields.push_back( fov );
	menu->chields.push_back( dist );
	menu->chields.push_back( new ImSeparator );
	menu->chields.push_back( minAngle );
	menu->chields.push_back( maxAngle );
	menu->chields.push_back( new ImSeparator );
	menu->chields.push_back( hiMul );
	menu->chields.push_back( hiSub );

	// Активация меню
	g_class.events->addCode( "BCBCBC\xA2" );
	g_class.events->onCode += [this]( std::string code ) {
		if ( code != "BCBCBC\xA2" ) return;
		menu->toggle( menu->isHidden() );
		g_class.cursor->toggleCursor( menu->isShowed() );
	};
}

AsiPlugin::~AsiPlugin() {
	// Destructor
	g_class.events->onMainLoop -= mid;
	for ( auto &ch : menu->chields ) delete ch;
	delete menu;
	auto [preReset, postReset, draw] = loaderSlots;
	g_class.DirectX->onPreReset -= preReset;
	g_class.DirectX->onPostReset -= postReset;
	g_class.DirectX->onDraw -= draw;
	ImGui::ImLoader::DeleteInstance();
}
