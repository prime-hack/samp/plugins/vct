#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <ImLoader.h>
#include <SRDescent.h>
#include <ciniserialization.h>

class AsiPlugin : public SRDescent, public CIniSerialization {
	std::tuple<size_t, size_t, size_t> loaderSlots;

	class ImMenu *menu;
	size_t		  mid;

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	SERIALIZE_EXT( bool, restore, true )
	SERIALIZE_EXT( float, max_fov, 100 )
	SERIALIZE_EXT( float, distance, 1 )
	SERIALIZE_EXT( float, Angle__min, 0.7853981853 )
	SERIALIZE_EXT( float, Angle__max, 1.553343058 )
	SERIALIZE_EXT( float, Height__mul, 1.3 )
	SERIALIZE_EXT( float, Height__sub, 0.4 )
};

#endif // MAIN_H
